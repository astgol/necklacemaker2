﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entities;

namespace NecklaceUI
{

    public partial class AddEditForm : Form
    {
        string _type = null;
        AbstractStone _stone = null;
        Necklace _necklace = null;

        public Necklace AsdA { get; set; }
        public AddEditForm()
        {
            InitializeComponent();
            _necklace= Data.Necklace; 
        }
        public AddEditForm(AbstractStone stone)
        {
            InitializeComponent();
            _stone = stone;
            string type;
            type = Convert.ToString(stone.GetType());
            string[] split = type.Split('.');
            type = split[1];
            _type = type;
            FillFields(type);
        }
        private void FillFields(string type)
        {
            DisableFields(type);
            comboBox1.Enabled = false;
            switch(type)
            {                    
                case "Beryl":
                    {
                        Beryl beryl = (Beryl)_stone;                        
                        nameTextBox.Text = beryl.Name;
                        colorTextBox.Text = beryl.Color;
                        weightTextBox.Text = beryl.Weight.ToString();
                        priceTextBox.Text = beryl.Price.ToString();
                        transpTextBox.Text = beryl.Transparency;
                        hardnessTextBox.Text = beryl.Hardness.ToString();
                        crystalTextBox.Text = beryl.CrystalSystem;
                    } break;
                case "Garnet":
                    {
                        Garnet garnet = (Garnet)_stone;
                        nameTextBox.Text = garnet.Name;
                        colorTextBox.Text = garnet.Color;
                        weightTextBox.Text = garnet.Weight.ToString();
                        priceTextBox.Text = garnet.Price.ToString();
                        transpTextBox.Text = garnet.Transparency;
                        hardnessTextBox.Text = garnet.Hardness.ToString();
                        fractTextBox.Text = garnet.Fracture;
                    } break;
                case "Silica":
                    {
                        Silica silica = (Silica)_stone;
                        nameTextBox.Text = silica.Name;
                        colorTextBox.Text = silica.Color;
                        weightTextBox.Text = silica.Weight.ToString();
                        priceTextBox.Text = silica.Price.ToString();
                        shineTextBox.Text = silica.Shine;
                        densityTextBox.Text = silica.Density.ToString();
                        tempTextBox.Text = silica.MeltingTemperature.ToString();
                    } break;
            }
        }
        private void comboBox1_SelectedValueChanged(object sender, EventArgs e)
        {
            string type = comboBox1.SelectedItem.ToString();
            DisableFields(type);
        }
        void DisableFields(string type)
        {
            EnableAllFields();
            switch (type)
            {
                case "Beryl":
                    {
                        shineTextBox.Enabled = false;
                        densityTextBox.Enabled = false;
                        fractTextBox.Enabled = false;
                        tempTextBox.Enabled = false;
                    } break;
                case "Garnet":
                    {
                        shineTextBox.Enabled = false;
                        densityTextBox.Enabled = false;
                        crystalTextBox.Enabled = false;
                        tempTextBox.Enabled = false;
                    } break;
                case "Silica":
                    {
                        transpTextBox.Enabled = false;
                        hardnessTextBox.Enabled = false;
                        crystalTextBox.Enabled = false;
                        fractTextBox.Enabled = false;
                    } break;
            }
        }
        void EnableAllFields()
        {
            transpTextBox.Enabled = true;
            hardnessTextBox.Enabled = true;
            densityTextBox.Enabled = true;
            crystalTextBox.Enabled = true;
            fractTextBox.Enabled = true;
            tempTextBox.Enabled = true;
            shineTextBox.Enabled = true;
        } 
        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        private void okButton_Click(object sender, EventArgs e)
        {
            string type = comboBox1.SelectedItem.ToString();
            switch (type)
            {
                case "Beryl":
                    {
                        string name = nameTextBox.Text;
                        double price = Convert.ToDouble(priceTextBox.Text);
                        double weight = Convert.ToDouble(weightTextBox.Text);
                        string crystal = crystalTextBox.Text;
                        string trans = transpTextBox.Text;
                        string color = colorTextBox.Text;
                        double hardness = Convert.ToDouble(hardnessTextBox.Text);
                        _stone = new Beryl(crystal, trans, hardness, name, color, price, weight);
                        _necklace.AddStone(_stone);
                    } break;
                case "Garnet":
                    {
                        string name = nameTextBox.Text;
                        double price = Convert.ToDouble(priceTextBox.Text);
                        double weight = Convert.ToDouble(weightTextBox.Text);
                        string fracture = fractTextBox.Text;
                        string trans = transpTextBox.Text;
                        string color = colorTextBox.Text;
                        double hardness = Convert.ToDouble(hardnessTextBox.Text);
                        _stone = new Garnet(fracture, trans, hardness, name, color, price, weight);
                        _necklace.AddStone(_stone);
                    } break;
                case "Silica":
                    {
                        string name = nameTextBox.Text;
                        double price = Convert.ToDouble(priceTextBox.Text);
                        double weight = Convert.ToDouble(weightTextBox.Text);
                        string color = colorTextBox.Text;
                        string shine = shineTextBox.Text;
                        double density = Convert.ToDouble(densityTextBox.Text);
                        double temp = Convert.ToDouble(tempTextBox.Text);
                        _stone = new Silica(temp, shine, density, name, color, price, weight);
                        _necklace.AddStone(_stone);
                    } break;
            }            
            this.Close();
        }
        private void editButton_Click(object sender, EventArgs e)
        {
            switch (_type)
            {
                case "Beryl":
                    {
                        Beryl beryl = (Beryl)_stone;
                        beryl.Name = nameTextBox.Text;
                        beryl.Price = Convert.ToDouble(priceTextBox.Text);
                        beryl.Weight = Convert.ToDouble(weightTextBox.Text);
                        beryl.CrystalSystem = crystalTextBox.Text;
                        beryl.Transparency = transpTextBox.Text;
                        beryl.Color = colorTextBox.Text;
                        beryl.Hardness = Convert.ToDouble(hardnessTextBox.Text);
                        //_necklace.AddStone(beryl);
                    } break;
                case "Garnet":
                    {
                        Garnet garnet = (Garnet)_stone;
                        garnet.Name = nameTextBox.Text;
                        garnet.Price = Convert.ToDouble(priceTextBox.Text);
                        garnet.Weight = Convert.ToDouble(weightTextBox.Text);
                        garnet.Fracture = fractTextBox.Text;
                        garnet.Transparency = transpTextBox.Text;
                        garnet.Color = colorTextBox.Text;
                        garnet.Hardness = Convert.ToDouble(hardnessTextBox.Text);

                    } break;
                case "Silica":
                    {
                        Silica silica = (Silica)_stone;
                        silica.Name = nameTextBox.Text;
                        silica.Price = Convert.ToDouble(priceTextBox.Text);
                        silica.Weight = Convert.ToDouble(weightTextBox.Text);
                        silica.MeltingTemperature = Convert.ToDouble(tempTextBox.Text);
                        silica.Density = Convert.ToDouble(densityTextBox.Text);
                        silica.Color = colorTextBox.Text;
                        silica.Shine = shineTextBox.Text;
                    } break;
            }
            this.Close();
        }      
    }
}
