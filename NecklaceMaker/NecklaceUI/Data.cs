﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities;
using Logic;

namespace NecklaceUI
{
    public class Data
    {
        static Necklace _necklace = NecklaceFactory.CreateNecklace();

        public static Necklace Necklace
        {
            get { return _necklace; }
            set { _necklace = value; }
        }
    }
}
