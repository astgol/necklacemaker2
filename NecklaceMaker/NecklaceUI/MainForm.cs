﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entities;
using Logic;

namespace NecklaceUI
{
    public partial class MainForm : Form
    {
        Necklace bestNecklace = Data.Necklace;
        public MainForm()
        {
            InitializeComponent();

        }

        private void addButton_Click(object sender, EventArgs e)
        {
            AddEditForm addForm = new AddEditForm();
            //addForm.Owner = this;
            addForm.ShowDialog();
            stonesListBox.DataSource = null;
            stonesListBox.DataSource = bestNecklace.Stones;
            stonesListBox.DisplayMember = "Name";
            this.Refresh();
            UpdateLabels();
            //okButton.Enabled = false;
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            stonesListBox.DataSource = bestNecklace.Stones;
            stonesListBox.DisplayMember = "Name";
            UpdateLabels();
        }
        private void UpdateLabels()
        {
            priceLabel.Text = String.Format("Price = {0}", NecklaceCalculator.GetTotalPrice(bestNecklace).ToString());
            weightLabel.Text = String.Format("Weight = {0}", NecklaceCalculator.GetTotalWeight(bestNecklace).ToString());
        }
        private void stonesList_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void closeButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void editButton_Click(object sender, EventArgs e)
        {

            AddEditForm form = new AddEditForm((AbstractStone)stonesListBox.SelectedItem);
            form.ShowDialog();
            stonesListBox.DataSource = null;
            stonesListBox.DataSource = bestNecklace.Stones;
            stonesListBox.DisplayMember = "Name";
            this.Refresh();
            UpdateLabels();

        }

      

       
    }
}
