﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Logic;
using Entities;

namespace NecklaceMakerConsole
{
    class NecklacePrinter
    {
        public static void Print(Necklace necklace)
        {
            double totalPrice = NecklaceCalculator.GetTotalPrice(necklace);
            double totalWeight = NecklaceCalculator.GetTotalWeight(necklace);
            Console.WriteLine("Цена = {0}, Вес = {1}", totalPrice, totalWeight);
        }
            }
}
