﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Logic;
using Entities;

namespace NecklaceMakerConsole
{
    class Program
    {

        static void Main(string[] args)
        {
            Necklace bestNecklace = NecklaceFactory.CreateNecklace();
            NecklacePrinter.Print(bestNecklace);
            Console.ReadKey();
           }
        }
    }
