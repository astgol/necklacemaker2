﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities;

namespace Logic
{
    public static class NecklaceFactory
    {
        public static Necklace CreateNecklace()
        {
            Necklace neckl = new Necklace();
            Beryl emerald = new Beryl("шестиугольная", "Прозрачный", 7.5, "Изумруд", "Зеленый", 430.0, 0.5);
            Beryl aquamarine = new Beryl("гексагональная", "прозрачный", 7.5, "Аквамарин", "голубовато-зелёный", 420.0, 1.5);
            Silica agate = new Silica(1713.0, "Матовый", 2.6, "Агат", "Вся палитра", 245.0, 2.1);
            Silica citrine = new Silica(1728.0, "Стеклянный", 2.65, "Цитрин", "Желтый", 1040.0, 2.5);

            neckl.AddStone(emerald);
            neckl.AddStone(aquamarine);
            neckl.AddStone(agate);
            neckl.AddStone(citrine);

            return neckl;
        }


        
    }
}
