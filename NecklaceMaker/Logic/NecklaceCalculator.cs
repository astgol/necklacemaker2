﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entities;

namespace Logic
{
    public static class NecklaceCalculator
    {
        public static double GetTotalPrice(Necklace neckl)
        {
            double totalPrice = 0;
            List<AbstractStone> stones = neckl.Stones;
            foreach (AbstractStone stone in stones)
            {
                totalPrice += stone.Price;
            }
            return totalPrice;
        }
        public static double GetTotalWeight(Necklace neckl)
        {
            double totalWeight = 0;
            List<AbstractStone> stones = neckl.Stones;
            foreach (AbstractStone stone in stones)
            {
                totalWeight += stone.Weight;
            }
            return totalWeight;
        }
    }
}
