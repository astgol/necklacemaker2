﻿namespace Entities
{
    public class Garnet : PreciousStone
    {
        private string _fracture;
        public string Fracture
        {
            get { return _fracture; }
            set { _fracture = value; }
        }
        public Garnet(string fract, string trans, double hard, string name, string color, double price, double weight)
            : base(hard, trans, name, color, price, weight)
        {
            this._fracture = fract;
        }
    }
}
