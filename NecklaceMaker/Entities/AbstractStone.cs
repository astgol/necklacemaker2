﻿
namespace Entities
{
    public abstract class AbstractStone
    {
        private string _name;
        private double _price;
        private string _color;
        private double _weight;

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
        public string Color
        {
            get { return _color; }
            set { _color = value; }
        }
        public double Weight
        {
            get { return _weight; }
            set { _weight = value; }
        }
        public double Price
        {
            get { return _price; }
            set { _price = value; }
        }
        public AbstractStone(string name, string color, double weight, double price)
        {
            this._name = name;
            this._color = color;
            this._price = price;
            this._weight = weight;

        }
    }
}
