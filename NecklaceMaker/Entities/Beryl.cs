﻿namespace Entities
{
    public class Beryl : PreciousStone
    {
        private string _crystalsystem;
        public string CrystalSystem
        {
            get { return _crystalsystem; }
            set { _crystalsystem = value; }
        }

        public Beryl(string crystal, string trans, double hardness, string name, string color, double price, double weight):base(hardness,trans,name, color, price, weight)
        {
            this._crystalsystem = crystal;
        }



    }
}
