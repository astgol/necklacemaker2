﻿
namespace Entities
{
    public class PreciousStone : AbstractStone
    {
        private string _transparency;
        private double _hardness;

        public string Transparency
        {
            get { return _transparency; }
            set { _transparency = value; }
        }
        public double Hardness
        {
            get { return _hardness; }
            set { _hardness = value; }
        }
        public PreciousStone(double hard, string trans, string name, string color, double price, double weight)
            : base(name, color, weight, price)
        {
            this._hardness = hard;
            this._transparency = trans;
        }
    }
}
