﻿
namespace Entities
{
    public class SemipreciousStone : AbstractStone
    {
        private string _shine;
        private double _density;

        public string Shine
        {
            get { return _shine; }
            set { _shine = value; }
        }
        public double Density
        {
            get { return _density; }
            set { _density = value; }
        }
        public SemipreciousStone(string shine, double density, string name, string color, double price, double weight)
            : base(name, color, weight, price)
        {
            this._shine = shine;
            this._density = density;
        }

    }
}
