﻿using System.Collections.Generic;

namespace Entities
{
    public class Necklace
    {
        private List<AbstractStone> _stones= new List<AbstractStone>();
        public void AddStone(AbstractStone stone)
        {
            _stones.Add(stone);
        }
        public void DeleteStone(AbstractStone stone)
        {
            _stones.Remove(stone);
        }
        public List<AbstractStone> Stones 
        { 
            get { return _stones; } 
        }
    }
}
