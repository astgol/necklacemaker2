﻿namespace Entities
{
    public class Silica : SemipreciousStone
    {
        private double _meltingtemperature;
        public double MeltingTemperature
        {
            get { return _meltingtemperature; }
            set { _meltingtemperature = value; }
        }
        public Silica(double temp, string shine, double density, string name, string color, double price, double weight)
            : base(shine, density, name, color, price, weight)
        {
            this._meltingtemperature = temp;
        }
    }
}
